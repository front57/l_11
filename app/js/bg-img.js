const imgs = document.querySelectorAll('.background-img');

imgs.forEach(e => {
    const imgSrc = e.src;
    e.parentElement.style.backgroundImage = `url('${imgSrc}')`;
    e.style.display = 'none';
});
