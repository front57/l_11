
function updateMapWidth() {
    const map = document.querySelector('.contacts__map');
    const infoBlock = document.querySelector('.contacts__info');
    const contactsRow = document.querySelector('.contacts__row');
    const contentOuter = document.querySelector('.contacts .content__outer');

    if(window.innerWidth > 576){
        map.style.maxWidth = contentOuter.offsetWidth - infoBlock.offsetWidth - (contentOuter.offsetWidth -
            contactsRow.clientWidth)/2 + 'px';
    }else{
        map.style.maxWidth = '100%';
    }
}
window.addEventListener('load', updateMapWidth);
window.addEventListener('resize', updateMapWidth);