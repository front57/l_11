(function () {

    const acc = document.querySelectorAll(".accordion__btn");
    acc.forEach(e => e.addEventListener('click', function () {
        const accBody = this.nextElementSibling;
        if(window.innerWidth < 381){
            accBody.style.maxHeight = accBody.style.maxHeight ? null : accBody.scrollHeight + 'px';
            this.classList.toggle('active');
            accBody.classList.toggle('active');
        }
    }));
})();

(function () {
    const accBtn = document.querySelectorAll('.accordion__header');
    accBtn.forEach(e => e.addEventListener('click', updateDisplayItem));
    window.addEventListener('load', activateDefaultTab);
    window.addEventListener('resize', activateDefaultTab);

    function updateDisplayItem(){
        const currentItem = this.parentElement;
        const currentContent = this.nextElementSibling;
        if(window.innerWidth > 768){
            displayNewContent(currentContent);
            clearActiveItems(currentItem);
            currentItem.classList.add('active');
        }else{
            currentContent.style.maxHeight = currentContent.style.maxHeight ? null : currentContent.scrollHeight + 'px';
            clearActiveItems(currentItem)
            currentItem.classList.toggle('active');

        }
    }

    function clearActiveItems(cItem) {
        const activeItems = document.querySelectorAll('.accordion__item');
        if(window.innerWidth > 768){
            activeItems.forEach(e => {
                if (e !== cItem){
                    e.classList.remove('active');
                }
            })
        }else{
            activeItems.forEach(e =>{
                if(e !== cItem){
                    e.classList.remove('active');
                    e.querySelector('.accordion__body').style.maxHeight = null;
                }
            })
        }
    }


    function displayNewContent(bodyContent) {
        const dispArea = document.querySelector('.accordion__display-content');
        dispArea.innerHTML = bodyContent.innerHTML;
    }

    function activateDefaultTab() {
        const activeItem = document.querySelector('.accordion__item.active');
        const dispArea = document.querySelector('.accordion__display-content');
        const accItem = document.querySelector('.accordion__item');
        if(window.innerWidth > 768){
            if(activeItem === null){
                //const accItem = document.querySelector('.accordion__item');
                accItem.classList.add('active');
                dispArea.innerHTML = accItem.querySelector('.accordion__body').innerHTML;
            }else{
                dispArea.innerHTML = activeItem.querySelector('.accordion__body').innerHTML;
            }
        }else{
            if(activeItem === null){
                const accBody = accItem.querySelector('.accordion__body');
                accItem.classList.add('active');
                accBody.style.maxHeight = accBody.scrollHeight + 'px';
            }else{
                const accBody = activeItem.querySelector('.accordion__body');
                clearActiveItems(activeItem);
                accBody.style.maxHeight = accBody.scrollHeight + 'px';
            }
        }
    }
})();

(function () {
    window.addEventListener('resize', updDisplayMinHeight);
    window.addEventListener('load', updDisplayMinHeight);

    function updDisplayMinHeight(){
        if(window.innerWidth > 768){
            const accordion = document.querySelector('.accordion');
            const accDisplayContent = document.querySelector('.accordion__display-content');
            accDisplayContent.style.minHeight = accordion.offsetHeight + 'px';
        }
    }
})();
/*

(function () {
    const acc = document.querySelectorAll(".accordion__header");
    acc.forEach(e => e.addEventListener('click', updateAccItem));
    window.addEventListener('load', setDefaultActiveItem);
    window.addEventListener('resize', setDefaultActiveItem);

    function updateAccItem() {
        const accItem = this.parentElement;
        const accBody = this.nextElementSibling;
        if (innerWidth > 768){
            updateDispContent(accBody);
            clearAccItem(accItem)
            accItem.classList.add('active');
        }else{
            accBody.style.maxHeight = accBody.style.maxHeight ? null : accBody.scrollHeight + 'px';
            clearAccItem(accItem);
            accItem.classList.toggle('active');
        }
    }

    function clearAccItem(currentItem) {
        const accItems = document.querySelectorAll('.accordion__item.active');
        if (window.innerWidth < 768){
            accItems.forEach(e => {
                if ( e !== currentItem ){
                    e.classList.remove('active');
                    e.querySelector('.accordion__body').style.maxHeight = null;
                }
            });
        }else{
            accItems.forEach(e => e.classList.remove('active'));
        }
    }

    function updateDispContent(accBody) {
        const displayItem = document.querySelector('.accordion__display-content');
        displayItem.innerHTML = accBody.innerHTML;
    }
    function setDefaultActiveItem(){
        const accItem = document.querySelector('.accordion__item');
        const activeItem = document.querySelector('.accordion__item.active');
        const accDisplayContent = document.querySelector('.accordion__display-content');

        if (window.innerWidth > 768){
            if(activeItem === null){
                accItem.classList.add('active');
                accDisplayContent.innerHTML =
                    accItem.querySelector('.accordion__body').innerHTML;
            }else{
                accDisplayContent.innerHTML = activeItem.querySelector('.accordion__body').innerHTML;
            }
        }else{
            if(activeItem === null){
                accItem.classList.add('active');
                const accBody = accItem.querySelector('.accordion__body');
                accBody.style.maxHeight = accBody.scrollHeight + 'px';
            }else{
                //accItem.forEach(e => e.querySelector('.accordion__body').style.maxHeight = null);
                const accBody = activeItem.querySelector('.accordion__body');

                clearAccItem(activeItem)
                accBody.style.maxHeight = accBody.scrollHeight + 'px';
            }
        }
    }
})();

(function () {
    window.addEventListener('resize', updDisplayMinHeight);
    window.addEventListener('load', updDisplayMinHeight);

    function updDisplayMinHeight(){
        if(window.innerWidth > 768){
            const accordion = document.querySelector('.accordion');
            const accDisplayContent = document.querySelector('.accordion__display-content');
            accDisplayContent.style.minHeight = accordion.offsetHeight + 'px';
        }
    }
})();
*/
