const modalBtns = document.querySelectorAll('.modal__btn');
const clsModal = document.querySelectorAll('.modal__close');
const modals = document.querySelectorAll('.modal');

modalBtns.forEach(e => e.addEventListener('click', function(){
    document.querySelector('.modal').classList.add('active')
}));

clsModal.forEach(e => e.addEventListener('click', function(){
    document.querySelector('.modal').classList.remove('active');
}));

window.addEventListener('click', function(e){
    if(e.target.classList.contains('modal__window')){
        modals.forEach(e => e.classList.remove('active'));
    }
});