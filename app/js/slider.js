const swiper = new Swiper(".hero__slider", {
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
});


(function () {
    const opts = {
        breakpoints: {
            576:{
                slidesPerView:2,
            },
            650: {
                slidesPerView: 3,
            },
            992:{
                slidesPerView: 4,
            }
        },

        spaceBetween:20,
        navigation: {
            nextEl: '.specialist__next-btn',
            prevEl: '.specialist__prev-btn',
        }
    };
    const breakpoint = window.matchMedia('(min-width: 576px)');
    let init = false;
    let specialistSwiper;

    const swiperMode = function () {
        if(breakpoint.matches === true){
            if (! init){
                specialistSwiper = new Swiper(".specialist__slider", opts);
                init = true;
            }
        }else if(breakpoint.matches === false){
            if (specialistSwiper !== undefined){
                specialistSwiper.destroy(true, true);
                init = false;
            }
        }
    }
    window.addEventListener('resize', swiperMode);
    window.addEventListener('load', swiperMode);
})();

(function () {
    const opts = {
        breakpoints: {
            320:{
              slidesPerView:4,
            },
            576:{
                slidesPerView:3,
            },
            768:{
                slidesPerView:4,
            }
        },
        spaceBetween:20,

    };
    const breakpoint = window.matchMedia('(min-width: 576px)');
    const breakpointMax = window.matchMedia('(max-width:768px)');
    let init = false;
    let licenseSwiper;

    const swiperModeLicense = function () {
        if(breakpoint.matches === true && breakpointMax.matches === true){

            if (! init){
                licenseSwiper = new Swiper(".license__slider", opts);
                init = true;
            }
        }else if(breakpoint.matches === false && breakpointMax.matches === false){
            if (licenseSwiper !== undefined){
                licenseSwiper.destroy(true, true);
                init = false;
            }
        }
    }
    window.addEventListener('resize', swiperModeLicense);
    window.addEventListener('load', swiperModeLicense);
})();