//-27.112822747828375, -109.37074561992965

function initMap() {

    const island = { lat: -27.112822747828375, lng: -109.37074561992965 };

    const map = new google.maps.Map(document.querySelector(".contacts__g-map"), {
        zoom: 11,
        center: island,
    });

    const marker = new google.maps.Marker({
        position: island,
        map: map,
    });
}

window.initMap = initMap;