function sleep(ms){
    return new Promise(resolve => setTimeout(resolve, ms));
}

const displayHeroTitle = function (entries, observer) {
    entries.forEach((entry) => {
        if(entry.isIntersecting){
            entry.target.classList.add('normalize-v-element');
            entry.target.nextElementSibling.classList.add('normalize-v-element');

            sleep(700).then(() => {
               entry.target.nextElementSibling.nextElementSibling.classList.add('normalize-element');
            });
        }
    })
}
const displayTitle = function (entries, observer) {
    entries.forEach((entry) => {
        if(entry.isIntersecting){
            setTimeout(()=>{
                entry.target.classList.add('normalize-h-element');
            }, 200);
        }
    });
}
const displayDirectionCards = function (entries, observer) {
    let sleep = 150;
    const currentWidth = window.innerWidth;
    entries.forEach((entry) => {
        if(entry.isIntersecting){
            setTimeout(function () {
                if(currentWidth > 768){
                    entry.target.classList.add('normalize-v-element');
                }else{
                    entry.target.classList.add('normalize-element');
                }
            }, sleep);
        }
        sleep += 200;
    });
}
const displayPromCard = function (entries, observer) {
    const width = window.innerWidth;
    entries.forEach(entry => {
        if (entry.isIntersecting){
            if(width > 768){
                setTimeout(function () {
                    entry.target.classList.add('normalize-v-element');
                }, 450);
                const smallCards = document.querySelectorAll('.prom__card--small');
                let sleep = 650;
                smallCards.forEach(e => {
                    setTimeout(function () {
                        e.classList.add('normalize-v-element');
                    }, sleep);
                    sleep += 200;
                });
            }else if (width <= 768 && width > 576){
                setTimeout(function () {
                    entry.target.classList.add('normalize-element');
                }, 450);
                const smallCards = document.querySelectorAll('.prom__card--small');
                let sleep = 650;
                smallCards.forEach(e => {
                    setTimeout(function () {
                        e.classList.add('normalize-element');
                    }, sleep);
                    sleep += 200;
                });
            }else{
                setTimeout(function () {
                    entry.target.classList.add('normalize-element');
                }, 450);
            }
        }
    });
}

const displayPromSmallCards = function (entries, observer) {
    const width = window.innerWidth;
    entries.forEach(entry => {
        if(entry.isIntersecting){
            if(width < 576){
                entry.target.classList.add('normalize-element');
            }
        }
    });
};

const displayHospitalCard = function (entries, observer) {
    let sleep = 400;
    const width = window.innerWidth;
    entries.forEach(entry => {
       if(entry.isIntersecting){
           setTimeout(function () {
               entry.target.classList.add('normalize-element');
           }, sleep);
       }
       sleep += width > 650 ? 250 : 450;
    });
}

const displayHospitalParagraph = function (entries, observer) {
    const width = window.innerWidth;
    let sleep = 450;
    entries.forEach(entry => {
        if (entry.isIntersecting){
            if (width < 650){
                setTimeout(function () {
                    entry.target.classList.add('normalize-element')
                }, sleep);
            }
        }
        sleep += 200;
    });

}
const displayHospitalImg = function (entries, observer) {
    let sleep = 400
    const width = window.innerWidth;
    entries.forEach(entry => {
        if(entry.isIntersecting){
            entry.target.classList.add('normalize-element');
            if (width > 650){
                document.querySelectorAll('.hospital__paragraph').forEach(e => {
                    setTimeout(function () {
                        e.classList.add('normalize-h-element');
                    }, sleep);
                    sleep += 200;
                });
            }
            setTimeout(function () {
                document.querySelector('.hospital__txt .hospital__btn')
                    .classList.add('normalize-element');
            }, sleep);
        }
    });
};

const displayEquipmentImg = function (entries, observer) {
    let sleep = 200;
    entries.forEach( (entry) => {
        if (entry.isIntersecting){
            setTimeout(function () {
                entry.target.classList.add('normalize-v-element');
            }, sleep);
        }
        sleep += 250;
    })
}

const opts = {};

const mainTitleObserver = new IntersectionObserver(displayHeroTitle, opts);
const mainTitle = document.querySelectorAll('.hero__title');
mainTitle.forEach(e => {
    mainTitleObserver.observe(e);
});

const globTitleObserver = new IntersectionObserver(displayTitle, opts);
const globTitle = document.querySelectorAll('.glob-title');
globTitle.forEach(e => {
    globTitleObserver.observe(e);
});

const directionCardsObserver = new IntersectionObserver(displayDirectionCards, opts);
const directionCards = document.querySelectorAll('.direction__card');
directionCards.forEach(e => {
    directionCardsObserver.observe(e);
});

const promCardObserver = new IntersectionObserver(displayPromCard, {threshold: .7});
const promCard = document.querySelector('.prom__card');
promCardObserver.observe(promCard);

const hospitalCardObserver = new IntersectionObserver(displayHospitalCard, opts);
const hospitalCards = document.querySelectorAll('.hospital__card');
hospitalCards.forEach(e => {
    hospitalCardObserver.observe(e);
});

const hospitalImgObserver = new IntersectionObserver(displayHospitalImg, {threshold: .6});
const hospitalImg = document.querySelector('.hospital__img-wrapper');
hospitalImgObserver.observe(hospitalImg);

const equipmentImgObserver = new IntersectionObserver(displayEquipmentImg, {threshold: .7});
const equipmentImg = document.querySelectorAll('.equipment__img');
equipmentImg.forEach(e => {
    equipmentImgObserver.observe(e);
})

const hospitalParagraphObserver = new IntersectionObserver(displayHospitalParagraph, opts);
const hospitalParagraphs = document.querySelectorAll('.hospital__paragraph');

hospitalParagraphs.forEach(e => {
    hospitalParagraphObserver.observe(e);
})



const promSmCardsObserver = new IntersectionObserver(displayPromSmallCards, opts);
const promSmCards = document.querySelectorAll('.prom__card--small');

promSmCards.forEach(e => {
    promSmCardsObserver.observe(e);
})